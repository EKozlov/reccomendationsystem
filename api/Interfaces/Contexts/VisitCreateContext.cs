﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public class VisitCreateContext : IContext
    {
        public VisitArticle VisitArticle { get; set; }
    }
}
