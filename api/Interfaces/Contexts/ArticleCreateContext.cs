﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public class ArticleCreateContext : IContext
    {
        public Article Article { get; set; }
    }
}
