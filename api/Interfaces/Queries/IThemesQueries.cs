﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Web.API.Models;

namespace Interfaces.Queries
{
    public interface IThemesQueries : IQueryBase< ThemeModel, ThemeFilter>
    {
    
    }
}
