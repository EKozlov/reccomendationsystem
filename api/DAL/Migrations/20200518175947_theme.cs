﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class theme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassificatorId",
                table: "Themes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Themes_ClassificatorId",
                table: "Themes",
                column: "ClassificatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Themes_Classificators_ClassificatorId",
                table: "Themes",
                column: "ClassificatorId",
                principalTable: "Classificators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Themes_Classificators_ClassificatorId",
                table: "Themes");

            migrationBuilder.DropIndex(
                name: "IX_Themes_ClassificatorId",
                table: "Themes");

            migrationBuilder.DropColumn(
                name: "ClassificatorId",
                table: "Themes");
        }
    }
}
