﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class classificators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ArticleId",
                table: "Classificators",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Classificators_ArticleId",
                table: "Classificators",
                column: "ArticleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classificators_Articles_ArticleId",
                table: "Classificators",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classificators_Articles_ArticleId",
                table: "Classificators");

            migrationBuilder.DropIndex(
                name: "IX_Classificators_ArticleId",
                table: "Classificators");

            migrationBuilder.DropColumn(
                name: "ArticleId",
                table: "Classificators");
        }
    }
}
