﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class update_articl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles");

            migrationBuilder.AlterColumn<string>(
                name: "PhotoUploadId",
                table: "Articles",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles",
                column: "PhotoUploadId",
                principalTable: "Uploads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles");

            migrationBuilder.AlterColumn<string>(
                name: "PhotoUploadId",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles",
                column: "PhotoUploadId",
                principalTable: "Uploads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
