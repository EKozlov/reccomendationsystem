﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class update_article : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhotoUploadId",
                table: "Articles",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_PhotoUploadId",
                table: "Articles",
                column: "PhotoUploadId");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles",
                column: "PhotoUploadId",
                principalTable: "Uploads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Uploads_PhotoUploadId",
                table: "Articles");

            migrationBuilder.DropIndex(
                name: "IX_Articles_PhotoUploadId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "PhotoUploadId",
                table: "Articles");
        }
    }
}
