﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class createclassificators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classificators_Articles_ArticleId",
                table: "Classificators");

            migrationBuilder.DropIndex(
                name: "IX_Classificators_ArticleId",
                table: "Classificators");

            migrationBuilder.DropColumn(
                name: "ArticleId",
                table: "Classificators");

            migrationBuilder.AddColumn<string>(
                name: "SourceForDocument",
                table: "Classificators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Classificator",
                table: "Articles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Articles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SourceForDocument",
                table: "Classificators");

            migrationBuilder.DropColumn(
                name: "Classificator",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Articles");

            migrationBuilder.AddColumn<int>(
                name: "ArticleId",
                table: "Classificators",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Classificators_ArticleId",
                table: "Classificators",
                column: "ArticleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classificators_Articles_ArticleId",
                table: "Classificators",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
