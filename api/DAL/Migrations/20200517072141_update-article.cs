﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class updatearticle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Classificator",
                table: "Articles");

            migrationBuilder.AddColumn<int>(
                name: "ClassificatorId",
                table: "Articles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Articles_ClassificatorId",
                table: "Articles",
                column: "ClassificatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Classificators_ClassificatorId",
                table: "Articles",
                column: "ClassificatorId",
                principalTable: "Classificators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Classificators_ClassificatorId",
                table: "Articles");

            migrationBuilder.DropIndex(
                name: "IX_Articles_ClassificatorId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "ClassificatorId",
                table: "Articles");

            migrationBuilder.AddColumn<string>(
                name: "Classificator",
                table: "Articles",
                nullable: true);
        }
    }
}
