﻿using DAL.Entities;
using DAL.EntitiesConfigurations;
using Microsoft.EntityFrameworkCore;
using System;

namespace DAL
{
    public class EfContext : DbContext
    {
        public EfContext(DbContextOptions<EfContext> options)
            : base(options)
        {
        }

        public DbSet<Upload> Uploads { get; set; }
        public DbSet<Entities.Login> Logins { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ArticlesClassificators> ArticlesClassificators { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Classificators> Classificators { get; set; }
        public DbSet<Theme> Themes { get; set; }

        public DbSet<UserArticles> UserArticles { get; set; }
        public DbSet<VisitArticle> VisitArticles { get; set; }

        public DbSet<UsersThemes> UsersThemes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UploadEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ArticleCLassificatorEntitiesConfiguration());
            modelBuilder.ApplyConfiguration(new ClassificatorsEntititesConfiguration());
            modelBuilder.ApplyConfiguration(new ArticleEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserThemesEntityConfiguration());
            modelBuilder.ApplyConfiguration(new VisitArticleEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ThemeEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserArticleEntityConfiguration());
        }
    }
}