﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    internal class ArticleEntityConfiguration : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne<Classificators>()
                .WithMany()
                .HasForeignKey(x => x.ClassificatorId).IsRequired();

            builder.HasOne<Upload>()
                .WithMany()
                .HasForeignKey(x => x.PhotoUploadId).IsRequired(false);

        }
    
    }
}
