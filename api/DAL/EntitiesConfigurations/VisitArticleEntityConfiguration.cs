﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    internal class VisitArticleEntityConfiguration : IEntityTypeConfiguration<VisitArticle>
    {
        public void Configure(EntityTypeBuilder<VisitArticle> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne<Article>()
                .WithMany()
                .HasForeignKey(x => x.ArticleId).IsRequired();
            builder.HasOne<User>()
                .WithMany()
                .HasForeignKey(x => x.UserId).IsRequired();

        }

    }
}
