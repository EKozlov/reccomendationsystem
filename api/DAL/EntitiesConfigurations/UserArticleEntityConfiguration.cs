﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    internal class UserArticleEntityConfiguration : IEntityTypeConfiguration<UserArticles>
    {
        public void Configure(EntityTypeBuilder<UserArticles> builder)
        {
            builder.HasKey(x => new { x.UserId, x.ArticleId });

            builder.ToTable("UsersArticles");

            builder.HasOne<User>()
                .WithMany()
                .HasForeignKey(x => x.UserId).IsRequired();

            builder.HasOne<Article>()
                .WithMany()
                .HasForeignKey(x => x.ArticleId).IsRequired();

        }
    
    }
}
