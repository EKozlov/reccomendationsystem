﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    class UserThemesEntityConfiguration : IEntityTypeConfiguration<UsersThemes>
    {
        public void Configure(EntityTypeBuilder<UsersThemes> builder)
        {
            builder.HasKey(x => new { x.UserId, x.ThemeId });

            builder.ToTable("UsersThemes");

            builder.HasOne<User>()
                .WithMany()
                .HasForeignKey(x => x.UserId).IsRequired();

            builder.HasOne<Theme>()
                .WithMany()
                .HasForeignKey(x => x.ThemeId).IsRequired();

        }
    
    }
}
