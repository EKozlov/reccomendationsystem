﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.API.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Size { get; set; }
        public int ClassificatorId { get; set; }
        public string Classificator { get; set; }
        public UploadModel Photo { get; set; }
        public string PhotoUploadId { get; set; }
        public string HtmlDocx { get; set; }

    }
}
