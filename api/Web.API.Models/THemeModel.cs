﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Web.API.Models
{
    public class ThemeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Classificators Classificators { get; set; }
        public int ClassificatorId { get; set; }
    }
}
