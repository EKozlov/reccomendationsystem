﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.API.Models
{
    public class ArticleFilter : FilterBase
    {
        public int? Popular { get; set; }
        public string Request { get; set; }
        public int? Reccomendation { get; set; }
        public int UserArticles { get; set; }
        public int UserId { get; set; }
    }
}
