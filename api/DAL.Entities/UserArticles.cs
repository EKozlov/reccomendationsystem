﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class UserArticles
    {
        public int UserId { get; set; }
        public int ArticleId { get; set; }
    }
}
