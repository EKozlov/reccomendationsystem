﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class VisitArticle
    {
        public int Id { get; set; }
        public DateTime VisitDate { get; set; }
        public int ArticleId { get; set; }
        public int UserId { get; set; }
    }
}
