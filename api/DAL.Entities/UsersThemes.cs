﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class UsersThemes
    {
        public int UserId { get; set; }
        public int ThemeId { get; set; }
    }
}
