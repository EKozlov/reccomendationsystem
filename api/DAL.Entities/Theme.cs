﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Theme
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassificatorId { get; set; }
    }
}
