﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Size { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ClassificatorId { get; set; }
        public string PhotoUploadId { get; set; }

    }
}
