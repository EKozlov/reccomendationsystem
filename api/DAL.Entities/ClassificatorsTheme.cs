﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class ClassificatorsTheme
    {
        public int Count { get; set; }
        public string Name { get; set; }
        public string Probability { get; set; }
    }
}
