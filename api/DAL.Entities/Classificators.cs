﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Classificators
    {
        public int Id { get; set; }
        public string Classificator { get; set; }
        public string Name { get; set; }
        public string Probability { get; set; }
        public string  SourceForDocument  { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
