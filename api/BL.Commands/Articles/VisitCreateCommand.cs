﻿using DAL;
using Interfaces;
using Interfaces.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.Commands.Articles
{
    public class VisitCreateCommand : ICommand<VisitCreateContext>
    {
        private readonly EfContext _db;

        public VisitCreateCommand(EfContext db)
        {
            _db = db;

        }

        public async Task<CommandResult> ExecuteAsync(VisitCreateContext context)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                _db.VisitArticles.Add(context.VisitArticle);
                await _db.SaveChangesAsync();
                transaction.Commit();
            }
            return CommandResult.Success();
        }
    
    }
}
