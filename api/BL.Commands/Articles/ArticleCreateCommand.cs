﻿using DAL;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Contexts;


namespace BL.Commands.Articles
{
    public class ArticleCreateCommand : ICommand<ArticleCreateContext>
    {
        private readonly EfContext _db;

        public ArticleCreateCommand(EfContext db)
        {
            _db = db;

        }

        public async Task<CommandResult> ExecuteAsync(ArticleCreateContext context)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                _db.Articles.Add(context.Article);
                await _db.SaveChangesAsync();
                transaction.Commit();
            }
            return CommandResult.Success();
        }
    }
}
