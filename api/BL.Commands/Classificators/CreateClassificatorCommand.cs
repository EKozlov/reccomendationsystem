﻿using ClassifierGen;
using DAL;
using DAL.Entities;
using Interfaces;
using Interfaces.Contexts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace BL.Commands.CreateArticle
{
    public class CreateClassificatorCommand : ICommand<ClassificatorCreateContext>
    {

        private readonly EfContext _db;
        List<ClassificatorsTheme> classificatorsThemes = new List<ClassificatorsTheme>();
        string regex = new Regex(@"[\p{IsCyrillic}]+").ToString();

        public CreateClassificatorCommand(EfContext db)
        {
            _db = db;
        }

        public async Task<CommandResult> ExecuteAsync(ClassificatorCreateContext context)
        {
            string BaseResultPath = Path.GetFullPath(@"..\Web.API\Documents\");

            var file = Directory.GetFiles(BaseResultPath).Where(x => x == context.source).FirstOrDefault();

            using (var document = DocX.Load(file))
            {
                if (document.FindUniqueByPattern(regex, RegexOptions.IgnoreCase).Count > 0)
                {
                    classificatorsThemes = new GenClassifier().Generate(document);
                }
            }


            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                await CreateClassificators(classificatorsThemes, context.source);
                transaction.Commit();
            }
            return CommandResult.Success();
        }

        public async Task CreateClassificators(List<ClassificatorsTheme> classificatorsThemes, string source)
        {
            var classificators = _db.Classificators.Where(x => x.SourceForDocument == source);
            _db.Classificators.RemoveRange(classificators);
            
            if (classificatorsThemes.Count != 0)
            {
                foreach (var item in classificatorsThemes)
                {
                    _db.Classificators.Add(new Classificators
                    {
                        Classificator = item.Name,
                        Name = item.Name,
                        Probability = item.Probability,
                        SourceForDocument = source,
                        CreatedDate = DateTime.UtcNow
                    });
                    await _db.SaveChangesAsync();
                }
            }
        }
    }
}
