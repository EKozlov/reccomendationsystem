﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ClassifierGen.Constants
{
    public static class Constants
    {
        public static string keywordsPath = Path.GetFullPath(@"..\ClassifierGen\KeyWords\Keywords.txt");

        public static string BaseResultPath = Path.GetFullPath(@"..\GenClassifier\mystem\mystem.exe");

        public static double tfMinValue = 7;

        public static string BinWordsPath = Path.GetFullPath(@"..\ClassifierGen\BinWords\BinWords.txt");

        public static int numberMatches = 3;

        public static string[] BinWords;

        public static readonly int CountVisit = 4;
    }
}
