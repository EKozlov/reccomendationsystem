﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DAL.Entities;
using Interfaces;
using Interfaces.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.API.Models;

namespace Web.API.Controllers
{
    [Route("api/themes")]
    [ApiController]
    public class ThemeController : ControllerBase
    {

        private readonly ICommandHandler _commandHandler;
        private readonly EfContext _db;
        private readonly IThemesQueries _themesQueries;
        private readonly IUsersQueries _usersQueries;


        public ThemeController(ICommandHandler commandHandler, EfContext db, IThemesQueries themesQueries, IUsersQueries usersQueries)
        {
            _commandHandler = commandHandler;
            _db = db;
            _themesQueries = themesQueries;
            _usersQueries = usersQueries;
        }

        [HttpGet("")]
        [ProducesResponseType(typeof(List<ThemeModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetList(
            int page = 1,
            int size = 1000,
            int time_offset = 0
        )
        {
            var result = await _themesQueries.GetList(new ThemeFilter
            {
                Page = page,
                Size = size,

            });
            return Ok(result);
        }

        [HttpGet("get-classificators")]
        public async Task<IActionResult> GetClassificators()
        {
            var result = await _db.Classificators.ToListAsync();
            return Ok(result);
        }



        [HttpPost("create")]
        public async Task<IActionResult> Post([FromBody] ThemeModel theme)
        {
            _db.Themes.Add(new Theme { Name = theme.Name, ClassificatorId = theme.ClassificatorId });
            await _db.SaveChangesAsync();

            return Ok(theme);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var element = _db.Themes.Where(x => x.Id == id).FirstOrDefault();
            _db.Themes.Remove(element);
            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("choice")]
        public async Task<IActionResult> ChoiseTheme([FromBody] List<ThemeModel> theme)
        {
            if (theme.Count == 0)
                return Ok();

            var user = await _usersQueries.GetByToken();

            var userthemes = _db.UsersThemes.Where(x => x.UserId == user.Id);
            _db.UsersThemes.RemoveRange(userthemes);
            foreach (var item in theme)
            {
                _db.UsersThemes.Add(new UsersThemes
                {
                    UserId = user.Id ?? 0,
                    ThemeId = item.Id
                });
                await _db.SaveChangesAsync();
            }
            return Ok(theme);
        }



    }


}