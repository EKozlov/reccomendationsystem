﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DAL.Entities;
using Interfaces;
using Interfaces.Contexts;
using Interfaces.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.API.Models;
using OpenXmlPowerTools;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Xml.Linq;

namespace Web.API.Controllers
{
    [Route("api/article")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly ICommandHandler _commandHandler;
        private readonly EfContext _db;
        private readonly IUsersQueries _usersQueries;
        private readonly IArticleQueries _articleQueries;

        public ArticleController(ICommandHandler commandHandler, EfContext db, IUsersQueries usersQueries, IArticleQueries articleQueries)
        {
            _commandHandler = commandHandler;
            _db = db;
            _usersQueries = usersQueries;
            _articleQueries = articleQueries;
        }


        [HttpPost("classificator")]
        public async Task<IActionResult> AddFile(IFormFile file)
        {
            string BaseResultPath = Path.GetFullPath(@"..\Web.API\Documents\");

            string path = BaseResultPath + file.FileName;

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            var commandResult = await _commandHandler.Execute(new ClassificatorCreateContext
            {
                source = path
            });

            List<Classificators> classificators = new List<Classificators>();
            if (commandResult.IsSuccess)
            {
                classificators = _db.Classificators
                   .Where(x => x.CreatedDate > x.CreatedDate.AddSeconds(-10) && x.SourceForDocument == path)
                   .ToList();
            }

            return Ok(classificators);
        }

        [HttpPost("CreateArticle")]
        public async Task<IActionResult> CreateArticle([FromBody] ArticleModel form)
        {
            var article = GetArticle(form);
            var commandResult = await _commandHandler.Execute(new ArticleCreateContext
            {
                Article = article
            });
            var kek = commandResult.IsSuccess;
            return Ok(article);
        }

        [HttpPost("visit")]
        public async Task<IActionResult> Visit([FromBody] VisitArticleModel model)
        {
            var user = await _usersQueries.GetByToken();
            var visit = GetVisitArticle(model, user.Id);
            var commandResult = await _commandHandler.Execute(new VisitCreateContext
            {
                VisitArticle = visit
            });
            var kek = commandResult.IsSuccess;
            return Ok(visit);
        }

        private VisitArticle GetVisitArticle(VisitArticleModel x, int? id)
        {
            return new VisitArticle
            {
                ArticleId = x.ArticleId,
                UserId = id ?? 0,
                VisitDate = DateTime.UtcNow
            };
        }

        [HttpGet("")]
        [ProducesResponseType(typeof(List<ThemeModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetList(
            int page = 1,
            int size = 1000,
            int popular = 0,
            string request = null,
            int reccomendation = 0,
            int userArticles = 0
        )
        {
            var user = await _usersQueries.GetByToken();
            var result = await _articleQueries.GetList(new ArticleFilter
            {
                Page = page,
                Size = size,
                Popular = popular,
                Request = request,
                Reccomendation = reccomendation,
                UserId = user.Id ?? 0,
                UserArticles = userArticles

            });
            return Ok(result);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int id)
        {
            var user = await _articleQueries.GetById(id);
            if (user == null) return BadRequest(new ApiResultCode("DOCUMENT_NOT_FOUND", "Документ не найден"));

            return Ok(user);
        }


        private Article GetArticle(ArticleModel articleModel)
        {
            return new Article
            {
                CreatedDate = DateTime.UtcNow,
                Source = articleModel.Source,
                Description = articleModel.Description,
                Name = articleModel.Name,
                Size = string.Empty,
                ClassificatorId = articleModel.ClassificatorId,
                PhotoUploadId = articleModel.PhotoUploadId
            };
        }










    }








}