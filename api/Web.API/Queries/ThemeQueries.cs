﻿using DAL;
using DAL.Entities;
using Interfaces.Queries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.API.Models;

namespace Web.API.Queries
{
    public class ThemeQueries : IThemesQueries
    {
        private readonly EfContext _db;
        public ThemeQueries(EfContext db)
        {
            _db = db;
        }
        public Task<ThemeModel> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<PageResultModel<ThemeModel>> GetList(ThemeFilter filter)
        {
            var query = _db.Themes.AsQueryable();





            var count = await query.CountAsync();

            query = query.OrderByDescending(x => x.Id)
                .Skip((filter.Page - 1) * filter.Size)
                .Take(filter.Size);

            var list = await MapQuery(query).ToListAsync();

            return new PageResultModel<ThemeModel>
            {
                Size = filter.Size,
                Page = filter.Page,
                TotalCount = count,
                List = list
            };
        }
        private IQueryable<ThemeModel> MapQuery(IQueryable<Theme> query)
        {
            return query.Select(x => new ThemeModel
            {
                Name = x.Name,
                Id = x.Id,
                Classificators = _db.Classificators.Where(z => z.Id == x.ClassificatorId)
                .Select(m => new Classificators
                {
                    CreatedDate = m.CreatedDate,
                    SourceForDocument = m.SourceForDocument,
                    Classificator = m.Classificator,
                    Name = m.Name,
                    Probability = m.Probability
                }).FirstOrDefault(),
                ClassificatorId = x.ClassificatorId
            });
        }
    }
}
