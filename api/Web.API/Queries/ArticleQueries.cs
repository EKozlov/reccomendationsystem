﻿using DAL;
using DAL.Entities;
using Interfaces.Queries;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.API.Models;
using OpenXmlPowerTools;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Xml.Linq;
using System.IO;

namespace Web.API.Queries
{
    public class ArticleQueries : IArticleQueries
    {
        private readonly EfContext _db;
        public ArticleQueries(EfContext db)
        {
            _db = db;
        }

        public async Task<ArticleModel> GetById(int id)
        {
            var query = _db.Articles.AsQueryable().Where(x => x.Id == id);

            return await MapQuery(query).FirstOrDefaultAsync();
        }

        public async Task<PageResultModel<ArticleModel>> GetList(ArticleFilter filter)
        {
            var query = _db.Articles.AsQueryable();

            if (filter.Popular == 1)
            {
                var visits = _db.VisitArticles.AsQueryable();

                var visitArticles = visits
                    .Where(x => x.VisitDate > x.VisitDate.AddDays(-7))
                    .GroupBy(
                    x => x.ArticleId,
                    x => x.ArticleId,
                    (id, items) => new
                    {
                        ArticleId = id,
                        Count = items.Count()
                    }).Where(z => z.Count > ClassifierGen.Constants.Constants.CountVisit);

                query = query.Where(x => visitArticles.Any(z => z.ArticleId == x.Id));

            }

            if (!String.IsNullOrWhiteSpace(filter.Request))
            {
                var searchRequest = filter.Request.ToLower().Split(' ');
                if (searchRequest.Length > 1)
                {
                    foreach (var item in searchRequest)
                    {
                        query = query.Where(x => x.Name.ToLower().Contains(item)
                        || x.Description.ToLower().Contains(item)
                        || x.Name.ToLower().StartsWith(item)
                        || x.Description.ToLower().StartsWith(item));
                    }
                }
                else
                {
                    query = query.Where(x => x.Name.ToLower() == searchRequest[0] || x.Description.ToLower() == searchRequest[0] ||
                    x.Name.ToLower().StartsWith(searchRequest[0]) || x.Description.ToLower().StartsWith(searchRequest[0]) ||
                    x.Name.ToLower().Contains(searchRequest[0]) || x.Description.ToLower().Contains(searchRequest[0]));
                }
            }
            if (filter.UserArticles == 1)
            {
                query = query.Where(x => _db.UserArticles.Any(z => z.UserId == filter.UserId));
            }

            if (filter.Reccomendation == 1)
            {
                var visits = _db.VisitArticles.Where(x => x.UserId == filter.UserId && x.VisitDate > x.VisitDate.AddDays(-3));
                if (visits.Count() == 0)
                {
                    query = query.Where(x => _db.UsersThemes.Any(p => p.UserId == filter.UserId && _db.Themes.Any(z => z.ClassificatorId == x.ClassificatorId && z.Id == p.ThemeId)));
                }
                else
                {
                    query = query.Where(x => visits
                                           .Any(z => z.ArticleId == x.Id && z.UserId == filter.UserId)
                                                        || (_db.UsersThemes.Any(p => p.UserId == filter.UserId &&
                                                                    _db.Themes.Any(z => z.ClassificatorId == x.ClassificatorId && z.Id == p.ThemeId))));
                }
            }

            var list = await MapQuery(query).ToListAsync();

            return new PageResultModel<ArticleModel>
            {
                Size = filter.Size,
                Page = filter.Page,
                List = list
            };
        }


        private IQueryable<ArticleModel> MapQuery(IQueryable<Article> query)
        {
            return query.Select(x => new ArticleModel
            {
                Description = x.Description,
                Source = x.Source,
                Id = x.Id,
                Name = x.Name,
                ClassificatorId = x.ClassificatorId,
                HtmlDocx = ConvertDocxToHtml.ConvertToHtml(x.Source),
                Size = x.Size,
                Classificator = _db.Classificators.FirstOrDefault(z => z.Id == x.ClassificatorId).Name,
                Photo = _db.Uploads.Where(ph => ph.Id == x.PhotoUploadId)
                .Select(u => new PhotoUploadModel
                {
                    Id = u.Id,
                    SizeInBytes = u.SizeInBytes,
                    Extension = u.Extension,
                    Width = u.Width,
                    Height = u.Height
                }).FirstOrDefault()
            });
        }
    }

    public static class ConvertDocxToHtml
    {
        public static string ConvertToHtml(string path)
        {
            string result = "";
            byte[] byteArray = File.ReadAllBytes(path);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                memoryStream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument doc = WordprocessingDocument.Open(memoryStream, true))
                {
                    HtmlConverterSettings settings = new HtmlConverterSettings()
                    {
                        PageTitle = "My Page Title"
                    };
                    XElement html = HtmlConverter.ConvertToHtml(doc, settings);
                    result = html.ToString();
                }
            }
            return result;
        }
    }
}
