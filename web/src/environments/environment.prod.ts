export const environment = {
  production: true,
  apiUrl: process.env.API_URL || 'https://localhost:44325/api'
};
