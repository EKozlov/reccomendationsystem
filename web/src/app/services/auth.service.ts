import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TokenName } from 'src/app/common/constants/auth.constant';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Route, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private cookieService: CookieService, private router: Router, private client: HttpClient) { }

  public user = new BehaviorSubject<any>(null);

  public get isAuth() {
    return this.cookieService.get(TokenName).length ? true : false;
  }

  public get getUserId() {
    return JSON.parse(window.atob(this.cookieService.get(TokenName).split('.')[1])).UserId;
  }

  public logout() {
    this.cookieService.delete(TokenName);
    this.router.navigateByUrl('/account/login');
  }

  public getUser() {
    return this.client.get("https://localhost:44325/api/users/token");
 }
}
