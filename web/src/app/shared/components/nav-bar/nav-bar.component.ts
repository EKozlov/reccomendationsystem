import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  user: any;
  city = "";
  isGetUser: boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthService, private cookieService: CookieService) {
  }

  get isAuth() {
    return this.authService.isAuth;
  }

  logout() {
    this.authService.logout();
  } 
  ngOnInit() {
    this.getUser();
}
  

  public getUser() {
    this.authService.getUser().subscribe(user => {
      this.user = user;
      this.isGetUser = true;
      console.log(user);
    })
  }


}
