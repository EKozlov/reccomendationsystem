import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MaterialModule } from '../material.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [NotFoundComponent, NavBarComponent, FooterComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    MaterialModule
  ],
  providers: [],
  exports: [NotFoundComponent, NavBarComponent, FooterComponent]
})
export class SharedModule { }
