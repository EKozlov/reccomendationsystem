import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ThemesService } from '../../themes.service';
import { Classificator } from '../../interfaces/classificator';


@Component({
    selector: 'app-themes',
    templateUrl: './themes.component.html',
    styleUrls: ['./themes.component.scss']
})
export class ThemesComponent implements OnInit {
    popularArticles: any;
    themeForm: FormGroup;
    isGet: boolean = false;
    lastArray: any;
    classificators: Array<Classificator>;
    constructor(private http: HttpClient, private toastr: ToastrService, private fb: FormBuilder,
        private ngxLoader: NgxUiLoaderService, private router: Router, private service: ThemesService) { }

    ngOnInit() {
        this._createForm();
        this.getClassifcators();
    }

    private _createForm() {
        this.themeForm = new FormGroup({
            name: new FormControl(null)
        })
    }

    private getClassifcators() {
        this.service.getClassificators().subscribe(x => {
            this.classificators = x;
            this.toTodo();
        });
    }
    public toTodo() {

        for (var i = 0; i < this.classificators.length; i++) {
            this.todo.push({
                name: this.classificators[i].classificator,
                id: this.classificators[i].id
            })
        }
    }

    todo = [

    ];

    done = [

    ];

    public onClick() {
        let form = {
            classificatorId:this.done[0].id,
            name: this.themeForm.value.name
        }
        this.ngxLoader.start();
        this.service.sendTheme(form)
            .pipe(finalize(() => {
                this.ngxLoader.stop()
            }))
            .subscribe(x => {
                this.toastr.info('Тема создана');
                this.router.navigateByUrl("/theme/list");
            });


    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.container.data.length == 1) {
            this.toastr.warning('К теме можно прикрепить лишь 1 классификатор');
        } else {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            } else {
                transferArrayItem(event.previousContainer.data,

                    event.container.data,
                    event.previousIndex,
                    event.currentIndex);
            }
        }
    }



}



