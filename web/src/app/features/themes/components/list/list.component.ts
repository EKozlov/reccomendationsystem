import { OnInit, Component, Renderer2, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { ThemesService } from '../../themes.service';
import { DOCUMENT } from '@angular/platform-browser';



@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    list: any;
    constructor(private http: HttpClient, private toastr: ToastrService, private fb: FormBuilder,
        private ngxLoader: NgxUiLoaderService, private router: Router, private service: ThemesService) {
    }

    ngOnInit() {
        this.getListThemes();

    }

    public onClick() {
        this.router.navigateByUrl("/theme/create"); 
    }
    public onDelete(id) {
        this.service.delete(id).subscribe(x => {
            this.toastr.info('Тема удалена');
            this.list = this.list.filter(x => x.id != id);
        },
            (error) => {
                
            })
    }
    public getListThemes() {
        this.service.getList().subscribe(x => {
            this.list = x.list;
        })
    }

}



