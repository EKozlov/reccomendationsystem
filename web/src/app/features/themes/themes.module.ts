
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Routes, RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel'; 
import { MaterialModule } from 'src/app/material.module';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ThemesRoutingModule } from './themes.routing.module';
import { ThemesComponent } from './components/create/themes.component';
import { ThemesService } from './themes.service';
import { ListComponent } from './components/list/list.component';



@NgModule({
  declarations: [ThemesComponent, ListComponent],
  imports: [  
    SlickCarouselModule,
    CommonModule,
    ScrollingModule,
    ThemesRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers:[ThemesService]
})
export class ThemesModule { }
