export interface Classificator {
    classificator: string,
    createdDate: Date,
    id: number,
    name: string,
    probability: string,
    sourceForDocument: string
}