import { CollectionViewer, DataSource, ListRange } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { TokenName } from 'src/app/common/constants/auth.constant';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})
export class ThemesService {


    constructor(private client: HttpClient) { }

    public getClassificators() {
        return this.client.get<any>(environment.apiUrl + "/themes/get-classificators")
    }


    public sendTheme(theme) {
        return this.client.post(environment.apiUrl + "/themes/create", theme);
    }

    public getList() {
        return this.client.get<any>(environment.apiUrl + "/themes");
    }

    public delete(id) {
        return this.client.delete(environment.apiUrl + "/themes/" + id)
    }


}
