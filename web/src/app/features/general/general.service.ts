import { CollectionViewer, DataSource, ListRange } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { TokenName } from 'src/app/common/constants/auth.constant';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})
export class GeneralService {


    constructor(private client: HttpClient) { }

    public getPopularArticles() {
        return this.client.get<any>(environment.apiUrl + "/article/?popular=1");
    }

    public getSearchResult(request) {
        return this.client.get<any>(environment.apiUrl + "/article/?request=" + request);
    }

    public getReccomendationREsult(){
        return this.client.get<any>(environment.apiUrl + "/article/?reccomendation=1")
    }

}
