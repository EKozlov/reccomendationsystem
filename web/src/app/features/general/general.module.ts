import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Routes, RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel'; 
import { MaterialModule } from 'src/app/material.module';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { GeneralService } from './general.service';
import { GeneralRoutingModule } from './general-routing.module';
import { GeneralComponent } from './component/general.component';


@NgModule({
  declarations: [GeneralComponent],
  imports: [  
    SlickCarouselModule,
    CommonModule,
    ScrollingModule,
    GeneralRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers:[GeneralService]
})
export class GeneralModule { }
