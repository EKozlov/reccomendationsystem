import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { GeneralService } from '../general.service';


@Component({
    selector: 'app-general',
    templateUrl: './general.component.html',
    styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
    popularArticles: any;
    isPopularPhoto: boolean = false;
    isRecPhoto: boolean = false;
    constructor(private http: HttpClient, private toastr: ToastrService, private fb: FormBuilder,
        private ngxLoader: NgxUiLoaderService, private router: Router, private service: GeneralService) { }
    searchForm: FormGroup;
    response: any;
    reccomendtion: any;
    isResponsed: boolean = false;
    ngOnInit() {
        this.getPopularArticles();
        this.getReccomendation();
        this._createForm();
    }


    private _createForm() {
        this.searchForm = new FormGroup({
            search: new FormControl(null),
        })
    }

    public getPopularArticles() {
        this.service.getPopularArticles().subscribe(res => {
            this.popularArticles = res.list;
            this.isPopularPhoto = true;
        })
    }
    recSource: any
    public getReccomendation() {
        this.service.getReccomendationREsult().subscribe(res => {
            this.reccomendtion = res.list;
            this.isRecPhoto = true;
        });
    }

    public onClick() {
        let request = this.searchForm.value['search'];
        this.ngxLoader.start();
        this.service.getSearchResult(request)
            .pipe(finalize(() => this.ngxLoader.stop()))
            .subscribe(x => {
                this.response = x.list;
                let kek = [];
                kek = x.list;
                console.log(x);
                this.isResponsed = true;
                this.toastr.info('По запросу: ' + '«' + request + '»' + ' найдено ' + kek.length + ' совпадений')
            },
                error => {
                    if (error) {
                        this.toastr.warning('Невалидные данные', 'Ошибка поиска');
                    }
                });
    }
    public onPick(id) {

        this.router.navigateByUrl("article/" + id);

    }

}



