import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Routes, RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel'; 
import { MaterialModule } from 'src/app/material.module';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { PersonalOfficeRoutingModule } from './personal-office.routing.module';
import { PersonalOfficeComponent } from './personal/personal-office.component';
import { PersonalOfficeService } from './personal-office.service';



@NgModule({
  declarations: [PersonalOfficeComponent],
  imports: [  
    SlickCarouselModule,
    CommonModule,
    ScrollingModule,
    PersonalOfficeRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers:[PersonalOfficeService]
})
export class PersonalOfficeModule { }
