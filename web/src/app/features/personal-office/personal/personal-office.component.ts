import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { PersonalOfficeService } from '../personal-office.service';



@Component({
    selector: 'app-personal-office',
    templateUrl: './personal-office.component.html',
    styleUrls: ['./personal-office.component.scss']
})
export class PersonalOfficeComponent implements OnInit {
    uesrArticles: any;
    isSelectLibrary: boolean = false;
    constructor(private http: HttpClient, private toastr: ToastrService,
        private ngxLoader: NgxUiLoaderService, private router: Router, private service: PersonalOfficeService) { }

    ngOnInit() {
        this.getUserArticles();
    }


    public getUserArticles() {
        this.service.getUserArticles().subscribe(x => {
            this.uesrArticles = x.list;
            console.log(x)
        })
    }
    public onClickLibrary() {
        this.isSelectLibrary = true;
    }
    public onPickArticle(id) {
        this.router.navigateByUrl("/article/" + id);
    }
}



