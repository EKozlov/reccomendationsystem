import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-article-create',
  templateUrl: './article-create.component.html',
  styleUrls: ['./article-create.component.scss']
})
export class ArticleCreateComponent implements OnInit {
  classificators: any;
  isGetClassificators: boolean = false;
  isPressButtonLoadFile: boolean = false;
  articleForm: FormGroup;
  selectClassificator: any;
  isPhotoGet: boolean = false;
  photo: any;
  titlePhoto: any;
  isSelectClassificator: boolean = false;
  title: any = "Добавить файл";
  constructor(private http: HttpClient, private router: Router, private articleService: ArticleService, private fb: FormBuilder,
    private ngxLoader: NgxUiLoaderService) { }

  ngOnInit() {
    this._createForm()
  }
  private _createForm() {
    this.articleForm = new FormGroup({
      name: new FormControl(null),
      description: new FormControl(null),
      classificatorId: new FormControl(0),
      source: new FormControl(null),
      photoUploadId: new FormControl(null)
    })
  }

  uploadFile(file: Blob | File) {
    this.ngxLoader.start();
    var formData = new FormData();
    let fileName = file[0].name;
    this.title = fileName;
    this.isPressButtonLoadFile = true;
    formData.append('file', file[0], fileName);
    this.articleService.sendDocument(formData)
      .pipe(finalize(() => this.ngxLoader.stop()))
      .subscribe(x => {
        this.classificators = x;
        console.log(x);
        this.isPressButtonLoadFile = false;
        this.isGetClassificators = true;
      },
        (error) => {
          console.log('Классификатор не создан', error)
        });

  }
  public pickClassificator(id, source, name) {
    this.selectClassificator = name;
    this.isSelectClassificator = true;
    this.articleForm.patchValue({ classificatorId: id });
    this.articleForm.patchValue({ source: source });
  }

  public onSendDocument() {
    this.articleService.createArticle(this.articleForm.value).subscribe(response => {
      this.router.navigateByUrl("/general");
    },
      (error) => {
        console.log('Документ не сохранен', error)
      });
  }
  public uploadPhoto(file: Blob | File) {
    this.ngxLoader.start();
    var formData = new FormData();
    let fileName = file[0].name;
    this.titlePhoto = fileName;
    this.isPressButtonLoadFile = true;
    formData.append('file', file[0], fileName);
    this.articleService.uploadPhoto(formData)
      .pipe(finalize(() => this.ngxLoader.stop()))
      .subscribe(x => {
        this.photo = x;
        console.log(this.photo)
        this.isPhotoGet = true;
        this.articleForm.patchValue({ photoUploadId: this.photo.id });
      },
        (error) => {
          console.log('Фото не добавлено', error)
        });

  }

}



