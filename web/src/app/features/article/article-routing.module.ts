import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { ArticleCreateComponent } from './article-create/article-create.component';
import { ArticleOpenComponent } from './article-open/article-open.component';
import { ArticleListComponent } from './article-list/article-list.component';

const routes: Routes = [
  {
    path: '',
    component: ArticleCreateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':id',
    component: ArticleOpenComponent,
  },
  {
    path: 'list',
    component: ArticleListComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
