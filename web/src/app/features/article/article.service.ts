import { CollectionViewer, DataSource, ListRange } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {


  constructor(private client: HttpClient) { }

  public sendDocument(formData) {
    return this.client.post("https://localhost:44325/api/article/classificator", formData);
  }

  public createArticle(form) {
    return this.client.post("https://localhost:44325/api/article/CreateArticle", form)
  }
  public visitDocument(model) {
    return this.client.post(environment.apiUrl + "/article/visit", model)
  }

  public getDocument(id: number) {
    return this.client.get<any>(environment.apiUrl + "/article/" + id)
  }

  public addUserDocument(x) {
    return this.client.post(environment.apiUrl + "/users/add-article", x);
  }
  public uploadPhoto(file) {
    return this.client.post(environment.apiUrl + "/uploads", file);
  }
}
