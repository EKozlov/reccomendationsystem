import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScrollingModule } from '@angular/cdk/scrolling';
import { Routes, RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ArticleService } from './article.service';
import { ArticleCreateComponent } from './article-create/article-create.component';
import { ArticleRoutingModule } from './article-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ArticleOpenComponent } from './article-open/article-open.component';
import { ArticleListComponent } from './article-list/article-list.component';




@NgModule({
  declarations: [ArticleCreateComponent, ArticleOpenComponent, ArticleListComponent],
  imports: [
    SlickCarouselModule,
    CommonModule,
    ScrollingModule,
    ArticleRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [ArticleService]
})
export class ArticleModule { }
