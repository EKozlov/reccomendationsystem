import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ArticleService } from '../article.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';




@Component({
    selector: 'app-article-open',
    templateUrl: './article-open.component.html',
    styleUrls: ['./article-open.component.scss']
})
export class ArticleOpenComponent implements OnInit {
    document: any;
    isGetDocument: boolean = false;
    id: any;
    constructor(private http: HttpClient, private router: Router, private articleService: ArticleService, private fb: FormBuilder,
        private ngxLoader: NgxUiLoaderService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.getDocumentById(this.id);

    }
    public getDocumentById(id) {
        this.ngxLoader.start();
        this.articleService.getDocument(id)
            .pipe(finalize(() => {
                this.ngxLoader.stop()
            })).subscribe(res => {
                this.document = res;
                console.log(this.document);
                this.isGetDocument = true;
                const innerHtml = document.getElementById("inner")
                innerHtml.innerHTML = this.document.htmlDocx;
                this.visitDocument();
            })
    }
    public visitDocument() {
        let visit = {
            articleId: this.document.id,
            userId: 0,
            visitDate: null,
            id: 0
        }
        this.articleService.visitDocument(visit).subscribe(x => {
        })
    }

    public onClick() {
        let dto = {
            id: this.id
        }
        this.articleService.addUserDocument(dto)
            .subscribe(() => {
            })
    }

}



