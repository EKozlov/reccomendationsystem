import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule'
  },
  {
    path: 'news',
    loadChildren: './news/news.module#NewsModule'
  },
  {
    path: 'article',
    loadChildren: './article/article.module#ArticleModule'
  },
  {
    path: 'preference',
    loadChildren: './preference-choice/preference.module#PreferenceModule'
  },
  {
    path: 'general',
    loadChildren: './general/general.module#GeneralModule'
  },
  {
    path: 'lk',
    loadChildren: './personal-office/personal-office.module#PersonalOfficeModule'
  },
  {
    path: 'theme',
    loadChildren: './themes/themes.module#ThemesModule'
  }

];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
