import { Component, OnInit } from '@angular/core';
import { Registration } from '../../models/registration.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AccountService } from '../../account.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { registrationSchema } from './registration.schema';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  photo: any;
  isPhotoGet: boolean = false;

  constructor(private ngxLoader: NgxUiLoaderService,
    private toastr: ToastrService,
    private accountService: AccountService,
    private router: Router) { }

  ngOnInit() {
    this.fields = registrationSchema;
  }
  uploadFile(file: Blob | File) {
    var formData = new FormData();
    let fileName = file[0].name;
    formData.append('file', file[0], fileName);
    this.accountService.sendPhoto(formData).subscribe(x => {
      this.photo = x;
      this.isPhotoGet = true;
    },
      (error) => {
        console.log('Фото не отправлено', error)
      });

  }



  submit() {
    if (this.registerForm.valid) {
      this.registerForm.addControl('photo', new FormControl('',Validators.required));
      this.registerForm.patchValue({ photo: this.photo.id })
      this.ngxLoader.start();
      const loginModel: Registration = Object.assign({}, this.registerForm.value);
      this.accountService.registration(loginModel)
        .pipe(finalize(() => this.ngxLoader.stop()))
        .subscribe(() => {
          this.registerForm.reset();
          this.router.navigate(['account', 'login']);
        },
          err => {
            if (err) {
              this.toastr.warning('Невалидные данные', 'Ошибка при регистрации');
            }
          });
    }
  }
}
