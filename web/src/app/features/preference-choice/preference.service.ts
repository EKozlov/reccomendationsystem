import { CollectionViewer, DataSource, ListRange } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { TokenName } from 'src/app/common/constants/auth.constant';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})
export class PreferenceService {


    constructor(private client: HttpClient, private cookieService: CookieService, ) { }

    public getThemes() {
        return this.client.get("https://localhost:44325/api/themes")
    }

    public sendChoiceThemes(themes: any) {
        return this.client.post("https://localhost:44325/api/themes/choice", themes);

        
    }

}
