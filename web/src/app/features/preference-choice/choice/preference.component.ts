import { OnInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Classificator } from '../interfaces/classificator';
import { PreferenceService } from '../preference.service';
import { AuthService } from 'src/app/services/auth.service';
import { finalize } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-preference',
    templateUrl: './preference.component.html',
    styleUrls: ['./preference.component.scss']
})
export class PreferenceComponent implements OnInit {
    themes: any;
    width = "115px";
    height = "115px";
    currentUserId: any;
    user: any;
    isGetThemes: boolean = false;
    constructor(private http: HttpClient, private toastr: ToastrService, private ngxLoader: NgxUiLoaderService, private router: Router, private preferenseService: PreferenceService, private auth: AuthService) { }
    choiceTehemes = [];
    ngOnInit() {
        this.getThemes();
    }
    public toggle(i, name, id) {
        var div = document.getElementById(i);
        let width = div.style.width;
        let height = div.style.height;
        if (width && height == this.width) {
            div.style.width = "100px";
            div.style.height = "100px";
            div.style.backgroundColor = "red";
            let index;
            for (let i = 0; i < this.choiceTehemes.length; i++)
                if (this.choiceTehemes[i].name == name)
                    index = i;
            this.choiceTehemes.splice(index, 1);
        } else {
            div.style.width = this.width;
            div.style.height = this.height;
            div.style.backgroundColor = "#C71585";
            this.choiceTehemes.push({ 'id': id, 'name': name });
        }
    }

    public getThemes() {
        this.preferenseService.getThemes().subscribe(x => {
            this.themes = x;
            console.log(x);
            this.isGetThemes = true;
        })
    }

    public onClick() {
        this.ngxLoader.start()
        this.preferenseService.sendChoiceThemes(this.choiceTehemes)
            .pipe(finalize(() => this.ngxLoader.stop()))
            .subscribe(res => {
                this.router.navigate(['general']);
                console.log("themes 200Ok");
            },
                error => {
                    if (error) {
                        this.toastr.warning('Невалидные данные', 'Ошибка при отправке');
                    }
                });
    }

}



