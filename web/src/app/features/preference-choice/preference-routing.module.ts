import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { PreferenceComponent } from './choice/preference.component';


const routes: Routes = [
  {
    path: '',
    component: PreferenceComponent,
    canActivate: [AuthGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    // AuthGuard
  ],
  exports: [RouterModule]
})
export class PreferenceRoutingModule { }
