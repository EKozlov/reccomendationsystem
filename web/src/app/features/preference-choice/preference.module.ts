import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Routes, RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel'; 
import { MaterialModule } from 'src/app/material.module';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { PreferenceComponent } from './choice/preference.component';
import { PreferenceRoutingModule } from './preference-routing.module';
import { PreferenceService } from './preference.service';
import { AuthService } from 'src/app/services/auth.service';





@NgModule({
  declarations: [PreferenceComponent],
  imports: [  
    SlickCarouselModule,
    CommonModule,
    ScrollingModule,
    PreferenceRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers:[PreferenceService, AuthService]
})
export class PreferenceModule { }
